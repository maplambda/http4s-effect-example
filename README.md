# Running effects with Http4s
This project is an example of using [Http4s](https://http4s.org/) to run effects as a HTTP API. The program is a function of ```A=>F[B]``` which given a ```PokemonStore``` will return a ```HttpApp``` capable of receiving a HTTP ```Request``` and returning a effect ```Response``` that will be executed when matched on.

## Program
The ```Endpoints.service``` program returns a random Pokemon name when called via HTTP GET. A Pokemon database is provided as a ```PokemonStore``` with a ```random``` method. The program is a ```Klesli A=>F[B]``` mapping of ```PokemonStore``` to ```HttpApp[IO]``` with the type 
signature ```Reader[PokemonStore, HttpApp[IO]]```.

### A => F[B]
An Effect type ```F[_]``` supporting the ability to encode a delayed computation by providing the typeclasses Http4s relies on is required. With the ```IO``` effect Monad from the [Cats](https://typelevel.org/cats/) library for typeclasses, ```F[B]``` has the type ```HttpApp[IO]```. To provide our ```PokemonStore``` to this effect we use a ```Kleisli``` datatype, ```Reader``` which  takes the parameter ```A``` of type ```PokemonStore```.

### Running the Reader effect
The ```Reader``` effect models a value that will yield a mapping of ```A=>F[B]``` when called via the ```run``` function. 

### Running the HttpApp effect
```HttpApp``` is a ```Kleisli``` function with the type ```Kleisli[F, Request[F], Response[F]]```. This is run for us by a Http4s server or can be called directly for unit testing.

```scala
class EndpointsTest extends FunSuite {
  test("Root call returns Pokemon") {
    val program = Endpoints.service
    val store = new PokemonStore(List(Pokemon("Pikachu")))
    assert(
      // PokemonStore => HttpApp[IO]
      program.run(store)
        // Request[IO] => IO[Response[IO]]
        .run(Request[IO](Method.GET, Uri.uri("/"))).>>=(_.body.compile.toVector.map(_.map(_.toChar).mkString("")))
        .unsafeRunSync()
        === "Wild PIKACHU appeared!")
  }

}
```
## Running the Server
A ```HttpApp``` can be run as a server using [Blaze](https://github.com/http4s/blaze). 

``` scala
object Main extends IOApp {

  def run(args: List[String]): IO[ExitCode] =
    BlazeClientBuilder[IO](global).resource.use { client =>
      // use `client` here and return an `IO`.
      // the client will be acquired and shut down
      // automatically each time the `IO` is run.
      PokemonStore.initialize(client) >>= (store => BlazeServerBuilder[IO]
        .bindHttp(8080, "localhost")
        .withHttpApp(Endpoints.service.run(store))
        .serve
        .compile
        .drain
        .as(ExitCode.Success))
    }
}
```

The Pokemon database for the server implementation is provided by calling the https://pokeapi.co/ service using the Http4s [HttpClient](https://http4s.org/v0.18/client/) and [Circe](https://circe.github.io/circe/) for JSON decoding.
