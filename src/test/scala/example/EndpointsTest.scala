package example

import cats.effect.IO
import cats.syntax.all._
import org.http4s.{Method, Request, Uri}
import org.scalatest.FunSuite

class EndpointsTest extends FunSuite {
  test("Root call returns Pokemon") {
    val program = Endpoints.service
    val store = new PokemonStore(List(Pokemon("Pikachu")))
    assert(
      // PokemonStore => HttpApp[F]
      program.run(store)
        // Request[IO] => IO[Response[IO]]
        .run(Request[IO](Method.GET, Uri.uri("/"))).>>=(_.body.compile.toVector.map(_.map(_.toChar).mkString("")))
        .unsafeRunSync()
        === "Wild PIKACHU appeared!")
  }

}
