package example

import cats.data.Reader
import cats.effect._
import cats.syntax.all._
import io.circe.generic.auto._
import org.http4s.circe._
import org.http4s.client._
import org.http4s.client.blaze._
import org.http4s.dsl.io._
import org.http4s.implicits._
import org.http4s.server.blaze._
import org.http4s.{EntityDecoder, HttpRoutes, _}

import scala.concurrent.ExecutionContext.global

case class Pokemon(name: String)

case class PokemonResource(results: List[Pokemon])

class PokemonStore(val items: List[Pokemon]) {
  def random: IO[Pokemon] = IO.delay(items(scala.util.Random.nextInt(items.size)))
}

object PokemonStore {
  def initialize(client: Client[IO]): IO[PokemonStore] = {
    implicit val jsonDecoder: EntityDecoder[IO, PokemonResource] = jsonOf[IO, PokemonResource]
    client.expect("https://pokeapi.co/api/v2/pokemon/?offset=0&limit=151")
      .map(x => new PokemonStore(x.results))
  }
}

object Endpoints {
  val service: Reader[PokemonStore, HttpApp[IO]] = Reader[PokemonStore, HttpApp[IO]](store =>
    HttpRoutes.of[IO] {
      case GET -> Root => store.random >>= (x => Ok(s"Wild ${x.name.toUpperCase()} appeared!"))
    }.orNotFound // call the OptionT getOrElse with Response.notFound and return total function of HttpApp
  )
}

object Main extends IOApp {

  def run(args: List[String]): IO[ExitCode] =
    BlazeClientBuilder[IO](global).resource.use { client =>
      // use `client` here and return an `IO`.
      // the client will be acquired and shut down
      // automatically each time the `IO` is run.
      PokemonStore.initialize(client) >>= (store => BlazeServerBuilder[IO]
        .bindHttp(8080, "localhost")
        .withHttpApp(Endpoints.service.run(store))
        .serve
        .compile
        .drain
        .as(ExitCode.Success))
    }
}